package pt.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@EnableAutoConfiguration
@SpringBootApplication (scanBasePackages={"pt.api"})
public class ApiTutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTutorialApplication.class, args);
	}
}
