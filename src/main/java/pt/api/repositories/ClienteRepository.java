package pt.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pt.api.entities.ClienteEntity;

@Repository
public interface ClienteRepository extends JpaRepository<ClienteEntity, Integer> {
	@Query (value="SELECT * FROM cliente WHERE nif=?1",nativeQuery=true)
	ClienteEntity pesquisaPorNIF (String NIF);
}
