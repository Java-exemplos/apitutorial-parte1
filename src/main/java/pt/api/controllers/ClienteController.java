package pt.api.controllers;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pt.api.entities.ClienteEntity;
import pt.api.repositories.ClienteRepository;

@RestController
public class ClienteController {

	@Autowired
	private ClienteRepository clienteRepository;

	// EndPoint #1
	@RequestMapping(value = "/api/v1/cliente/{id}", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	
	public ClienteEntity getByClienteId(
			HttpServletRequest request, 
			HttpServletResponse response,
			@PathVariable(value = "id") String id) throws Exception {
		// verificação do ID
		int idCliente = 0;
		try {
			idCliente = Integer.parseInt(id);
		} catch (Exception ex) {
			response.sendError(HttpStatus.BAD_REQUEST.value());
			return null;
		}
		// Fetch de um cliente por ID
		ClienteEntity cliente = clienteRepository.findOne(idCliente);
		if (cliente == null) {
			response.sendError(HttpStatus.NOT_FOUND.value());
			return null;
		} else {
			return cliente;
		}
	}

	// EndPoint #2
	@RequestMapping(value = "/api/v1/cliente/all", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	
	public ResponseEntity<Collection<ClienteEntity>> getAllClientes(
			HttpServletRequest request,
			HttpServletResponse response) {
		
		Collection<ClienteEntity> clientes = clienteRepository.findAll();
		return new ResponseEntity<Collection<ClienteEntity>>(clientes, HttpStatus.OK);
	}

	// EndPoint #3
	@RequestMapping(value = "/api/v1/cliente", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	
	public ClienteEntity getByClienteNIF(
			HttpServletRequest request, 
			HttpServletResponse response,
			@RequestParam(value = "nif") String nif) throws Exception {

		if (!nif.matches("[0-9]+") && nif.length() != 9) {
			response.sendError(HttpStatus.BAD_REQUEST.value());
			return null;
		}

		ClienteEntity cliente = clienteRepository.pesquisaPorNIF(nif);
		return cliente;
	}
	// EndPoint #4

	@RequestMapping(value = "/api/v1/cliente/save", 
			method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	
	public int saveCliente(
			HttpServletRequest request, 
			HttpServletResponse response, 
			@RequestBody ClienteEntity cliente)
			throws Exception {

		if (cliente.getId() != null) {
			response.sendError(HttpStatus.METHOD_NOT_ALLOWED.value());
		}

		try {
			clienteRepository.save(cliente);
		} catch (Exception ex) {
			response.sendError(HttpStatus.BAD_REQUEST.value());
		}

		return HttpStatus.CREATED.value();
	}

	// EndPoint #5
	
	@RequestMapping(value = "/api/v1/cliente/update",
			method = RequestMethod.PUT, 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	
	public int updateCliente(
			HttpServletRequest request, 
			HttpServletResponse response,
			@RequestBody ClienteEntity cliente) throws Exception {

		// verificar se não é nulo e se existe
		if (cliente.getId() == null || clienteRepository.findOne(cliente.getId()) == null) {
			response.sendError(HttpStatus.NOT_FOUND.value(), "Erro de ID");
			return 0;
		}

		try {
			clienteRepository.save(cliente);
		} catch (Exception ex) {
			response.sendError(HttpStatus.BAD_REQUEST.value(), "Erro de BD");
			return 0;
		}

		return HttpStatus.ACCEPTED.value();

	}
	
	// EndPoint #5
	@RequestMapping(value = "/api/v1/cliente/delete/{id}",
			method = RequestMethod.DELETE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	
	public int deleteCliente(
			HttpServletRequest request, 
			HttpServletResponse response,
			@PathVariable(value = "id") String clienteID) throws Exception {
		
		// verificação do ID
		int idCliente = 0;
		try {
			idCliente = Integer.parseInt(clienteID);
		} catch (Exception ex) {
			idCliente = 0;
			response.sendError(HttpStatus.BAD_REQUEST.value());
			return 0;
		}

		try{
			clienteRepository.delete(idCliente);
		}catch (Exception ex) {
			response.sendError(HttpStatus.BAD_REQUEST.value(), "Erro de BD");
			return 0;
		}
		return HttpStatus.OK.value();
	}
}

