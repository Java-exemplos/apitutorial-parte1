package services;

import java.util.Collection;

import pt.api.entities.ClienteEntity;

public interface ClienteService {
	Collection <ClienteEntity> findAll();
	ClienteEntity findOne (int clienteID);
	ClienteEntity create (ClienteEntity cliente);
	ClienteEntity update (ClienteEntity cliente);
	void delete (int clienteID);
}

