package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.api.entities.ClienteEntity;
import pt.api.repositories.ClienteRepository;

@Service
public class ClienteServiceBean implements ClienteService {

	@Autowired 
	private ClienteRepository clienteRepository;
	Collection <ClienteEntity> cliente=clienteRepository.findAll();

	@Override
	public Collection<ClienteEntity> findAll() {
		Collection <ClienteEntity> clientes=clienteRepository.findAll();
		return clientes;
	}

	@Override
	public ClienteEntity findOne(int clienteID) {
		ClienteEntity cliente=clienteRepository.findOne(clienteID);
		return cliente;
	}

	@Override
	public ClienteEntity create(ClienteEntity cliente) {
		if (cliente.getId() != null){
			// impossivel criar o cliente, pois o ID é definido automáticamente pelo MySQL e não manualmente.
			return null;
		}
		ClienteEntity clienteAGravar=clienteRepository.save(cliente);
		return clienteAGravar;
	}

	@Override
	public ClienteEntity update(ClienteEntity cliente) {
		ClienteEntity clienteJaExiste=clienteRepository.findOne(cliente.getId());
		if (clienteJaExiste==null) {
			//Não podemos actualizar um cliente não existente.
			return null;
		}
		ClienteEntity clienteAActualizar=clienteRepository.save(cliente);
		return clienteAActualizar;
	}

	@Override
	public void delete(int clienteID) {
		clienteRepository.delete(clienteID);
	}

}
